package com.a7medkenawy.dagger;


import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.Subcomponent;

@ActivityScope
//@Component(dependencies = AppComponent.class)
@Subcomponent
public interface CoffeeComponent {

    Coffee getCoffee();

    void inject(MainActivity activity);


    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        Builder sugar(@Named("sugar") int sugar);

        @BindsInstance
        Builder milk(@Named("milk") int sugar);

//        Builder appComponent(AppComponent component);

        CoffeeComponent build();
    }
}
