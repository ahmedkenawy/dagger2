package com.a7medkenawy.dagger;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    Coffee coffee, coffee2;
    private static final String TAG = "Ahmed Saad";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        AppComponent appComponent = ((MainApplication) getApplication()).getComponent();


        CoffeeComponent component = appComponent.getCoffeeComponentBuilder().sugar(3).milk(6).build();
        component.inject(this);

        Log.d(TAG, coffee.getCoffee());

        Log.d(TAG, "coffee: " + coffee+ " coffee2: " + coffee2);
        Log.d(TAG, "river1: " + coffee.river+ " river2: " + coffee2.river);
        Log.d(TAG, "farm1: " + coffee.farm+ " farm2: " + coffee2.farm);

    }
}