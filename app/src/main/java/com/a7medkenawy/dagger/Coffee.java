package com.a7medkenawy.dagger;

import android.util.Log;

import javax.inject.Inject;
import javax.inject.Named;

@ActivityScope
public class Coffee {

    @Inject
    Farm farm;

    River river;

    @Inject
    public Coffee(River river, @Named("sugar") int sugar,@Named("milk") int milk) {
        this.river=river;
        Log.d("Ahmed Saad", "sugar: "+sugar+" ,milk:"+milk);
    }


    public String getCoffee() {
        return farm.getBeans() + " " + river.getWater();
    }


    @Inject
    public void connectElectricity() {
        Log.d("Ahmed Saad", "connectElectricity: ");
    }


}
