package com.a7medkenawy.dagger;

import android.util.Log;

import javax.inject.Inject;

public class River {

    private static final String TAG = "Ahmed Saad";

    @Inject
    public River() {
        Log.d(TAG, "River: ");
    }


    public String getWater(){
        return "Water";
    }


}
