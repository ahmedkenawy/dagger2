package com.a7medkenawy.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RiverModule {


    @Singleton
    @Provides
    public  River ProvideRiver(){
        return new River();
    }


}
